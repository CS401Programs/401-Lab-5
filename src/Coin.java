/*
	Coin.java THIS IS THE ONLY FILE YOU HAND IN
	THERE IS NO MAIN METHOD IN THIS CLASS!
*/
import java.util.Random;
public class Coin
{

	private int NumHeads,NumTails;
	private Random rdm;
	
	public Coin(int seed) {
		this.rdm = new Random(seed);
		this.NumHeads = 0;
		this.NumTails = 0;
	}

	public int getNumHeads() {
		return NumHeads;
	}

	private void setNumHeads(int count) {
		if (count == 1)
			NumHeads++;
		else
			NumHeads = 0;
	}

	public int getNumTails() {
		return NumTails;
	}

	private void setNumTails(int count) {
		if (count == 1)
			NumTails++;
		else
			NumTails = 0;
	}
	
	public char flip() {
		int side = rdm.nextInt(2);
		
		if (side == 1) {
			setNumHeads(1);
			return 'H';
		}
			else {
				setNumTails(1);
				return 'T';
			}
	}

	public void reset(){
		setNumHeads(0);
		setNumTails(0);
	}
	
} // END COIN CLASS